<?php

namespace App\Repository;

use App\Entity\Crimes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Crimes>
 *
 * @method Crimes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Crimes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Crimes[]    findAll()
 * @method Crimes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CrimesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Crimes::class);
    }

    public function save(Crimes $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Crimes $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

 

     /**
    * @return Crimes[] Returns an array of Test objects
    */
   public function findByInsee($insee): array
   {
       return $this->createQueryBuilder('t')
           ->andWhere('t.insee = :insee')
           ->andWhere('t.faits != :delit')
           ->setParameter('insee', $insee)
           ->setParameter('delit', "NA")
           ->orderBy('t.id', 'ASC')
           //->setMaxResults(10)
           ->getQuery()
           ->getResult()
       ;
   }
}
