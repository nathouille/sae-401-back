<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Crimes;
use App\Entity\Maisons;
use App\Entity\Appartements;
use App\Entity\Communes;
use App\Repository\CrimesRepository;

class PrincipalController extends AbstractController
{
    #[Route('/loyer/{insee}', name: 'loyer')]
    public function loyers($insee, ManagerRegistry $doctrine, CrimesRepository $crimesRepository): JsonResponse
    {
        $repositoryappartements = $doctrine->getRepository(Appartements::class);
        $repositorymaisons = $doctrine->getRepository(Maisons::class);
        $repositorycommunes = $doctrine->getRepository(Communes::class);
        $appartements = $repositoryappartements->findOneBy(array('insee' => $insee));
        $maisons = $repositorymaisons->findOneBy(array('insee' => $insee));
        $communes = $repositorycommunes->findOneBy(array('insee' => $insee));

        $appartementsinsee = $appartements->insee;
        $maisonsinsee = $maisons->insee;
        $communeslat = $communes->latitude;
        $communeslong = $communes->longitude;
        $nomcommune = $communes->nomcommune;

        $average = ($maisons->loycarrema+$appartements->loycarreap)/2;

        $crimes = $crimesRepository->findByInsee($insee);
        $pop = $crimes[0]->pop;
        $faits = $crimes[0]->faits;
    
        // Vérifiez que $pop et $faits sont définis et ont des valeurs
        if ((!isset($pop) || $pop == 0) && (!isset($faits) || $faits == 0)) {
            $tauxDelits = 0;
        } else {
            $tauxDelits = ($faits / $pop) * 100; // calcul du taux de délits en pourcentage
        }

        if (($average>=14 && $average<=18) || ($tauxDelits>=8 && $tauxDelits<=12)) {
            $note = 3;
        }if (($average>=19 && $average<=23) || ($tauxDelits>=13 && $tauxDelits<=17)) {
            $note = 2;
        }if (($average>=24 && $average<=27) || ($tauxDelits>=18 && $tauxDelits<=22)) {
            $note = 1;
        }if (($average>=28) || ($tauxDelits>=23)) {
            $note = 0;
        }if (($average<=13 && $average>9) || ($tauxDelits>=3 && $tauxDelits<=7)) {
            $note = 4;
        }if (($average<=8) || ($tauxDelits<=2)) {
            $note = 5;
        }
        $data['average']=$average;
        $data['note']=$note;
        $data['crimes']=$crimes;
        $data['pop']=$pop;
        $data['faits']=$faits;
        $data['taux']=$tauxDelits;
        $data['nomcommune']=$nomcommune;


        return new JsonResponse([$data]);
    }

    #[Route('/loyermaisons/{insee}', name: 'loyermaisons')]
    public function loyersMaisons($insee, ManagerRegistry $doctrine): JsonResponse
    {
        $repositorymaisons = $doctrine->getRepository(Maisons::class);
        $repositorycommunes = $doctrine->getRepository(Communes::class);
        $maisons = $repositorymaisons->findOneBy(array('insee' => $insee));
        $communes = $repositorycommunes->findOneBy(array('insee' => $insee));

        $maisonsinsee = $maisons->insee;
        $communeslat = $communes->latitude;
        $communeslong = $communes->longitude;
        $nomcommune = $communes->nomcommune;
        $prixmaisons = $maisons->loycarrema;

        return new JsonResponse([$prixmaisons,$maisonsinsee,$communeslat,$communeslong,$nomcommune]);
    }

    #[Route('/loyerappartements/{insee}', name: 'loyerAppartements')]
    public function loyersAppartements($insee, ManagerRegistry $doctrine): JsonResponse
    {
        $repositoryappartements = $doctrine->getRepository(Appartements::class);
        $repositorycommunes = $doctrine->getRepository(Communes::class);
        $appartements = $repositoryappartements->findOneBy(array('insee' => $insee));
        $communes = $repositorycommunes->findOneBy(array('insee' => $insee));

        $communeslat = $communes->latitude;
        $communeslong = $communes->longitude;
        $nomcommune = $communes->nomcommune;
        $prixappartements = $appartements->loycarreap;
        $appartementsinsee = $appartements->insee;

        return new JsonResponse([$prixappartements,$appartementsinsee,$communeslat,$communeslong,$nomcommune]);
    }

    #[Route('/loyerdepartement/{dep}', name: 'loyerdepartement')]
    public function loyersDepartement($dep, ManagerRegistry $doctrine, CrimesRepository $crimesRepository): JsonResponse
    {
        $repositoryappartements = $doctrine->getRepository(Appartements::class);
        $repositorymaisons = $doctrine->getRepository(Maisons::class);
        $appartements = $repositoryappartements->findBy(array('dep' => $dep));
        $maisons = $repositorymaisons->findBy(array('dep' => $dep));

        $totalma = 0;
        $countma = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($maisons as $maison) {
            $totalma += $maison->getLoycarrema();
            $countma++;
        }

        $moyennema = $totalma / $countma;

        $totalap = 0;
        $countap = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($appartements as $appartement) {
            $totalap += $appartement->getLoycarreap();
            $countap++;
        }

        $moyenneap = $totalap / $countap;

        $moyennett = ($moyenneap + $moyennema)/2;
        
        if ($moyennett>=14 || $moyennett<=18) {
            $note = 3;
        }if ($moyennett>=19 || $moyennett<=23) {
            $note = 2;
        }if ($moyennett>=24 || $moyennett<=27) {
            $note = 1;
        }if ($moyennett>=28) {
            $note = 0;
        }if ($moyennett<=13 || $moyennett>9) {
            $note = 4;
        }if ($moyennett<=8){
            $note = 5;
        }

        $data['moyennett'] = $moyennett;
        $data['note'] = $note;
        

        return new JsonResponse($data);
    }

    #[Route('/loyermaisonsdepartement/{dep}', name: 'loyermaisonsdepartement')]
    public function loyersMaisonsDepartement($dep, ManagerRegistry $doctrine): JsonResponse
    {
        $repositorymaisons = $doctrine->getRepository(Maisons::class);
        $maisons = $repositorymaisons->findBy(array('dep' => $dep));

        $total = 0;
        $count = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($maisons as $maison) {
            $total += $maison->getLoycarrema();
            $count++;
        }

        $moyenne = $total / $count;

        // Retourner la moyenne
        return new JsonResponse($moyenne);
    }

        

    #[Route('/loyerappartementsdepartement/{dep}', name: 'loyerappartementsdepartement')]
    public function loyersAppartementsDepartement($dep, ManagerRegistry $doctrine): JsonResponse
    {
        $repositoryappartements = $doctrine->getRepository(Appartements::class);
        $appartements = $repositoryappartements->findBy(array('dep' => $dep));


        $total = 0;
        $count = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($appartements as $appartement) {
            $total += $appartement->getLoycarreap();
            $count++;
        }

        $moyenne = $total / $count;

        // Retourner la moyenne
        return new JsonResponse($moyenne);
    }

    #[Route('/loyerregion/{reg}', name: 'loyerregion')]
    public function loyersRegion($reg, ManagerRegistry $doctrine): JsonResponse
    {
        $repositoryappartements = $doctrine->getRepository(Appartements::class);
        $repositorymaisons = $doctrine->getRepository(Maisons::class);
        $appartements = $repositoryappartements->findBy(array('reg' => $reg));
        $maisons = $repositorymaisons->findBy(array('reg' => $reg));

        $totalma = 0;
        $countma = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($maisons as $maison) {
            $totalma += $maison->getLoycarrema();
            $countma++;
        }

        $moyennema = $totalma / $countma;

        $totalap = 0;
        $countap = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($appartements as $appartement) {
            $totalap += $appartement->getLoycarreap();
            $countap++;
        }

        $moyenneap = $totalap / $countap;

        $moyennett = ($moyenneap + $moyennema)/2;

        return new JsonResponse($moyennett);
    }

    #[Route('/loyermaisonsregion/{reg}', name: 'loyermaisonsregion')]
    public function loyersMaisonsRegion($reg, ManagerRegistry $doctrine): JsonResponse
    {
        $repositorymaisons = $doctrine->getRepository(Maisons::class);
        $maisons = $repositorymaisons->findBy(array('reg' => $reg));

        $total = 0;
        $count = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($maisons as $maison) {
            $total += $maison->getLoycarrema();
            $count++;
        }

        $moyenne = $total / $count;

        // Retourner la moyenne
        return new JsonResponse($moyenne);
    }

        

    #[Route('/loyerappartementsregion/{reg}', name: 'loyerappartementsregion')]
    public function loyersAppartementsRegion($reg, ManagerRegistry $doctrine): JsonResponse
    {
        $repositoryappartements = $doctrine->getRepository(Appartements::class);
        $appartements = $repositoryappartements->findBy(array('reg' => $reg));


        $total = 0;
        $count = 0;

        // Itérer sur chaque enregistrement pour calculer la moyenne
        foreach ($appartements as $appartement) {
            $total += $appartement->getLoycarreap();
            $count++;
        }

        $moyenne = $total / $count;

        // Retourner la moyenne
        return new JsonResponse($moyenne);
    }

}
