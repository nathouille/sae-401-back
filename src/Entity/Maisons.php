<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Maisons
 *
 * @ORM\Table(name="Maisons")
 * @ORM\Entity
 */
class Maisons
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="insee", type="integer", nullable=true)
     */
    public $insee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LIBGEO", type="text", nullable=true)
     */
    public $libgeo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DEP", type="integer", nullable=true)
     */
    public $dep;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REG", type="integer", nullable=true)
     */
    public $reg;

    /**
     * @var float|null
     *
     * @ORM\Column(name="loycarrema", type="float", precision=10, scale=0, nullable=true)
     */
    public $loycarrema;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInsee(): ?int
    {
        return $this->insee;
    }

    public function setInsee(?int $insee): self
    {
        $this->insee = $insee;

        return $this;
    }

    public function getLibgeo(): ?string
    {
        return $this->libgeo;
    }

    public function setLibgeo(?string $libgeo): self
    {
        $this->libgeo = $libgeo;

        return $this;
    }

    public function getDep(): ?int
    {
        return $this->dep;
    }

    public function setDep(?int $dep): self
    {
        $this->dep = $dep;

        return $this;
    }

    public function getReg(): ?int
    {
        return $this->reg;
    }

    public function setReg(?int $reg): self
    {
        $this->reg = $reg;

        return $this;
    }

    public function getLoycarrema(): ?float
    {
        return $this->loycarrema;
    }

    public function setLoycarrema(?float $loycarrema): self
    {
        $this->loycarrema = $loycarrema;

        return $this;
    }


}
