<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Communes
 *
 * @ORM\Table(name="communes")
 * @ORM\Entity
 */
class Communes
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="insee", type="integer", nullable=true)
     */
    public $insee;

    /**
     * @var float|null
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=true)
     */
    public $latitude;

    /**
     * @var float|null
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=true)
     */
    public $longitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomcommune", type="text", nullable=true)
     */
    public $nomcommune;

    /**
     * @var int|null
     *
     * @ORM\Column(name="codedepartement", type="integer", nullable=true)
     */
    public $codedepartement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomdepartement", type="text", nullable=true)
     */
    public $nomdepartement;

    /**
     * @var int|null
     *
     * @ORM\Column(name="coderegion", type="integer", nullable=true)
     */
    public $coderegion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomregion", type="text", nullable=true)
     */
    public $nomregion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInsee(): ?int
    {
        return $this->insee;
    }

    public function setInsee(?int $insee): self
    {
        $this->insee = $insee;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getNomcommune(): ?string
    {
        return $this->nomcommune;
    }

    public function setNomcommune(?string $nomcommune): self
    {
        $this->nomcommune = $nomcommune;

        return $this;
    }

    public function getCodedepartement(): ?int
    {
        return $this->codedepartement;
    }

    public function setCodedepartement(?int $codedepartement): self
    {
        $this->codedepartement = $codedepartement;

        return $this;
    }

    public function getNomdepartement(): ?string
    {
        return $this->nomdepartement;
    }

    public function setNomdepartement(?string $nomdepartement): self
    {
        $this->nomdepartement = $nomdepartement;

        return $this;
    }

    public function getCoderegion(): ?int
    {
        return $this->coderegion;
    }

    public function setCoderegion(?int $coderegion): self
    {
        $this->coderegion = $coderegion;

        return $this;
    }

    public function getNomregion(): ?string
    {
        return $this->nomregion;
    }

    public function setNomregion(?string $nomregion): self
    {
        $this->nomregion = $nomregion;

        return $this;
    }


}
