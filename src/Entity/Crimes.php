<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Crimes
 *
 * @ORM\Table(name="Crimes")
 * @ORM\Entity
 */
class Crimes
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="insee", type="integer", nullable=true)
     */
    public $insee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="classe", type="text", nullable=true)
     */
    public $classe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valeur", type="text", nullable=true)
     */
    public $valeur;

    /**
     * @var string|null
     *
     * @ORM\Column(name="faits", type="text", nullable=true)
     */
    public $faits;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tauxpourmille", type="text", nullable=true)
     */
    public $tauxpourmille;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complementinfoval", type="text", nullable=true)
     */
    public $complementinfoval;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complementinfotaux", type="text", nullable=true)
     */
    public $complementinfotaux;

    /**
     * @var int|null
     *
     * @ORM\Column(name="POP", type="integer", nullable=true)
     */
    public $pop;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LOG", type="text", nullable=true)
     */
    public $log;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInsee(): ?int
    {
        return $this->insee;
    }

    public function setInsee(?int $insee): self
    {
        $this->insee = $insee;

        return $this;
    }

    public function getClasse(): ?string
    {
        return $this->classe;
    }

    public function setClasse(?string $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(?string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getFaits(): ?string
    {
        return $this->faits;
    }

    public function setFaits(?string $faits): self
    {
        $this->faits = $faits;

        return $this;
    }

    public function getTauxpourmille(): ?string
    {
        return $this->tauxpourmille;
    }

    public function setTauxpourmille(?string $tauxpourmille): self
    {
        $this->tauxpourmille = $tauxpourmille;

        return $this;
    }

    public function getComplementinfoval(): ?string
    {
        return $this->complementinfoval;
    }

    public function setComplementinfoval(?string $complementinfoval): self
    {
        $this->complementinfoval = $complementinfoval;

        return $this;
    }

    public function getComplementinfotaux(): ?string
    {
        return $this->complementinfotaux;
    }

    public function setComplementinfotaux(?string $complementinfotaux): self
    {
        $this->complementinfotaux = $complementinfotaux;

        return $this;
    }

    public function getPop(): ?int
    {
        return $this->pop;
    }

    public function setPop(?int $pop): self
    {
        $this->pop = $pop;

        return $this;
    }

    public function getLog(): ?string
    {
        return $this->log;
    }

    public function setLog(?string $log): self
    {
        $this->log = $log;

        return $this;
    }


}
