<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Appartements
 *
 * @ORM\Table(name="Appartements")
 * @ORM\Entity
 */
class Appartements
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="INSEE", type="integer", nullable=true)
     */
    public $insee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LIBGEO", type="text", nullable=true)
     */
    public $libgeo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DEP", type="integer", nullable=true)
     */
    public $dep;

    /**
     * @var int|null
     *
     * @ORM\Column(name="REG", type="integer", nullable=true)
     */
    public $reg;

    /**
     * @var float|null
     *
     * @ORM\Column(name="loycarreap", type="float", precision=10, scale=0, nullable=true)
     */
    public $loycarreap;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInsee(): ?int
    {
        return $this->insee;
    }

    public function setInsee(?int $insee): self
    {
        $this->insee = $insee;

        return $this;
    }

    public function getLibgeo(): ?string
    {
        return $this->libgeo;
    }

    public function setLibgeo(?string $libgeo): self
    {
        $this->libgeo = $libgeo;

        return $this;
    }

    public function getDep(): ?int
    {
        return $this->dep;
    }

    public function setDep(?int $dep): self
    {
        $this->dep = $dep;

        return $this;
    }

    public function getReg(): ?int
    {
        return $this->reg;
    }

    public function setReg(?int $reg): self
    {
        $this->reg = $reg;

        return $this;
    }

    public function getLoycarreap(): ?float
    {
        return $this->loycarreap;
    }

    public function setLoycarreap(?float $loycarreap): self
    {
        $this->loycarreap = $loycarreap;

        return $this;
    }


}
