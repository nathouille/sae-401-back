<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230318104026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Appartements AS SELECT id, INSEE, LIBGEO, DEP, REG, loycarreap FROM Appartements');
        $this->addSql('DROP TABLE Appartements');
        $this->addSql('CREATE TABLE Appartements (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, INSEE INTEGER DEFAULT NULL, LIBGEO CLOB DEFAULT NULL, DEP INTEGER DEFAULT NULL, REG INTEGER DEFAULT NULL, loycarreap DOUBLE PRECISION DEFAULT NULL)');
        $this->addSql('INSERT INTO Appartements (id, INSEE, LIBGEO, DEP, REG, loycarreap) SELECT id, INSEE, LIBGEO, DEP, REG, loycarreap FROM __temp__Appartements');
        $this->addSql('DROP TABLE __temp__Appartements');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Crimes AS SELECT id, insee, classe, valeur, faits, tauxpourmille, complementinfoval, complementinfotaux, POP, LOG FROM Crimes');
        $this->addSql('DROP TABLE Crimes');
        $this->addSql('CREATE TABLE Crimes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, insee INTEGER DEFAULT NULL, classe CLOB DEFAULT NULL, valeur CLOB DEFAULT NULL, faits CLOB DEFAULT NULL, tauxpourmille CLOB DEFAULT NULL, complementinfoval CLOB DEFAULT NULL, complementinfotaux CLOB DEFAULT NULL, POP INTEGER DEFAULT NULL, LOG CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO Crimes (id, insee, classe, valeur, faits, tauxpourmille, complementinfoval, complementinfotaux, POP, LOG) SELECT id, insee, classe, valeur, faits, tauxpourmille, complementinfoval, complementinfotaux, POP, LOG FROM __temp__Crimes');
        $this->addSql('DROP TABLE __temp__Crimes');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Maisons AS SELECT id, insee, LIBGEO, DEP, REG, loycarrema FROM Maisons');
        $this->addSql('DROP TABLE Maisons');
        $this->addSql('CREATE TABLE Maisons (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, insee INTEGER DEFAULT NULL, LIBGEO CLOB DEFAULT NULL, DEP INTEGER DEFAULT NULL, REG INTEGER DEFAULT NULL, loycarrema DOUBLE PRECISION DEFAULT NULL)');
        $this->addSql('INSERT INTO Maisons (id, insee, LIBGEO, DEP, REG, loycarrema) SELECT id, insee, LIBGEO, DEP, REG, loycarrema FROM __temp__Maisons');
        $this->addSql('DROP TABLE __temp__Maisons');
        $this->addSql('CREATE TEMPORARY TABLE __temp__communes AS SELECT id, insee, latitude, longitude, nomcommune, codedepartement, nomdepartement, coderegion, nomregion FROM communes');
        $this->addSql('DROP TABLE communes');
        $this->addSql('CREATE TABLE communes (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, insee INTEGER DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, nomcommune CLOB DEFAULT NULL, codedepartement INTEGER DEFAULT NULL, nomdepartement CLOB DEFAULT NULL, coderegion INTEGER DEFAULT NULL, nomregion CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO communes (id, insee, latitude, longitude, nomcommune, codedepartement, nomdepartement, coderegion, nomregion) SELECT id, insee, latitude, longitude, nomcommune, codedepartement, nomdepartement, coderegion, nomregion FROM __temp__communes');
        $this->addSql('DROP TABLE __temp__communes');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Appartements AS SELECT id, INSEE, LIBGEO, DEP, REG, loycarreap FROM Appartements');
        $this->addSql('DROP TABLE Appartements');
        $this->addSql('CREATE TABLE Appartements (id INTEGER PRIMARY KEY AUTOINCREMENT DEFAULT NULL, INSEE INTEGER DEFAULT NULL, LIBGEO CLOB DEFAULT NULL, DEP INTEGER DEFAULT NULL, REG INTEGER DEFAULT NULL, loycarreap DOUBLE PRECISION DEFAULT NULL)');
        $this->addSql('INSERT INTO Appartements (id, INSEE, LIBGEO, DEP, REG, loycarreap) SELECT id, INSEE, LIBGEO, DEP, REG, loycarreap FROM __temp__Appartements');
        $this->addSql('DROP TABLE __temp__Appartements');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Crimes AS SELECT id, insee, classe, valeur, faits, tauxpourmille, complementinfoval, complementinfotaux, POP, LOG FROM Crimes');
        $this->addSql('DROP TABLE Crimes');
        $this->addSql('CREATE TABLE Crimes (id INTEGER PRIMARY KEY AUTOINCREMENT DEFAULT NULL, insee INTEGER DEFAULT NULL, classe CLOB DEFAULT NULL, valeur CLOB DEFAULT NULL, faits CLOB DEFAULT NULL, tauxpourmille DOUBLE PRECISION DEFAULT NULL, complementinfoval CLOB DEFAULT NULL, complementinfotaux CLOB DEFAULT NULL, POP INTEGER DEFAULT NULL, LOG CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO Crimes (id, insee, classe, valeur, faits, tauxpourmille, complementinfoval, complementinfotaux, POP, LOG) SELECT id, insee, classe, valeur, faits, tauxpourmille, complementinfoval, complementinfotaux, POP, LOG FROM __temp__Crimes');
        $this->addSql('DROP TABLE __temp__Crimes');
        $this->addSql('CREATE TEMPORARY TABLE __temp__Maisons AS SELECT id, insee, LIBGEO, DEP, REG, loycarrema FROM Maisons');
        $this->addSql('DROP TABLE Maisons');
        $this->addSql('CREATE TABLE Maisons (id INTEGER PRIMARY KEY AUTOINCREMENT DEFAULT NULL, insee INTEGER DEFAULT NULL, LIBGEO CLOB DEFAULT NULL, DEP INTEGER DEFAULT NULL, REG INTEGER DEFAULT NULL, loycarrema DOUBLE PRECISION DEFAULT NULL)');
        $this->addSql('INSERT INTO Maisons (id, insee, LIBGEO, DEP, REG, loycarrema) SELECT id, insee, LIBGEO, DEP, REG, loycarrema FROM __temp__Maisons');
        $this->addSql('DROP TABLE __temp__Maisons');
        $this->addSql('CREATE TEMPORARY TABLE __temp__communes AS SELECT id, insee, latitude, longitude, nomcommune, codedepartement, nomdepartement, coderegion, nomregion FROM communes');
        $this->addSql('DROP TABLE communes');
        $this->addSql('CREATE TABLE communes (id INTEGER PRIMARY KEY AUTOINCREMENT DEFAULT NULL, insee INTEGER DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, nomcommune CLOB DEFAULT NULL, codedepartement INTEGER DEFAULT NULL, nomdepartement CLOB DEFAULT NULL, coderegion INTEGER DEFAULT NULL, nomregion CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO communes (id, insee, latitude, longitude, nomcommune, codedepartement, nomdepartement, coderegion, nomregion) SELECT id, insee, latitude, longitude, nomcommune, codedepartement, nomdepartement, coderegion, nomregion FROM __temp__communes');
        $this->addSql('DROP TABLE __temp__communes');
    }
}
