3 jeux de donnés :
- crimanilité par commune en france
- indicateur des loyers des appartements par communes
- indicateur des loyers des maisons par communes

Le but avec le back est de créer les API qu'il va falloir afficher avec le front.

Données à afficher :
- Prix moyen des loyers par commune
- Prix loyers maisons par communes
- Prix loyers appartements par communes
- Prix moyen des loyers par département
- Prix loyers maisons par département
- Prix loyers appartements par département
- Prix moyen des loyers par région
- Prix loyers maisons par région
- Prix loyers appartements par région
- Type de crime
- Taux de crimes par communes
- Taux de crimes par région
- Taux de crimes par département


Créer un compte administrateur